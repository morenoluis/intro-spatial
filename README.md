# Este repositorio contiene

1. [Introducción a datos espaciales](#gis)
    
    1.1. Introducción a GIS
    
    1.2. Tipos de datos espaciales
    
    1.3. Formatos de datos GIS
    
    1.4. Sistemas de Coordenadas de Referencia (CRS)

2. [Librería sf](#sf)

    2.1. Introducción a `sf`
    
    2.2. Importar datos espaciales
    
    2.3. Geometría de un dato espacial
    
    2.3. Atributos de un dato espacial

3. [Visualización de datos espaciales](#ggplot)

    3.1. Mapas básicos

    3.2. Mapas temático

4. [Operaciones geométricas](#operation)

    4.1. Reproyectar CRS
    
    4.2. Clipping
    
    4.3. Join

    4.4. Buffer
    
    4.5. Largo y area de un objeto
    
    4.6. Distancias

    4.7. Centroides
